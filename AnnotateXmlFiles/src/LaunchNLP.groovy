#!/usr/bin/env groovy
import fileProcessing.*
import groovy.io.*
import java.io.IOException
import static java.lang.Integer.parseInt as asInteger
import java.lang.Integer.*


class LaunchNLP {

	static main(args) {
		
		def xmlDir
		def removeSplittedF
		
		def propertiesReader = new properties()
		
		
		//preprocess
		
		if (propertiesReader.getPropValues("splitInputFile") == "true"){
			def fic = new File(propertiesReader.getPropValues("inputFile"))
			def root = propertiesReader.getPropValues("rootNode")
			def splitElement = propertiesReader.getPropValues("splitNode")
			xmlDir = propertiesReader.getPropValues("fileDirectory")
			removeSplittedF = propertiesReader.getPropValues("removeSplittedFiles")
			new xml().splitFile(fic, root, splitElement, xmlDir)
		}
		
		if (propertiesReader.getPropValues("cleanInputFile") == "true"){
			xmlDir = propertiesReader.getPropValues("fileDirectory")
			def expressionToRemove = propertiesReader.getPropValues("expressionToRemove")
			new xml().cleanFile(new File(xmlDir), expressionToRemove)
		}
		
		//Annotation 
		
		if (propertiesReader.getPropValues("launchAnnotation")== "true"){
			//we take the last modified child of the input directory to test if it's a directory or a file
			def subDir = new File(propertiesReader.getPropValues("fileDirectory")).listFiles()?.sort { it.lastModified() }?.head().isDirectory()
			def inputDirFile 		
			def dataSourceType
			// Annotation plan to be used
			def annotationPlan = propertiesReader.getPropValues("annotationPlan")
			// Cartridge procedure to be used
			def procedure = propertiesReader.getPropValues("procedure")
			// Input directory path
			xmlDir = new File(propertiesReader.getPropValues("fileDirectory"))
			def dsType = propertiesReader.getPropValues("inputSourceType")
			def anotatedDocDir = new File(propertiesReader.getPropValues("anotatedDocDirectory"))
			def OpeSystem = propertiesReader.getPropValues("machineOS")
			if (anotatedDocDir != xmlDir){
				//xmlFileDir = new InputFile().cleanFile(xmlFileDir)
				def annotator = new lux(removeOutputNodes : propertiesReader.getPropValues("removeOutputNodes"), renameOutputFile : propertiesReader.getPropValues("renameOutputFiles"), machineId : propertiesReader.getPropValues("machineId"))
				//annotator.loadGrapes()
				if (annotator.getRenameOutputFile() == "true"){
					if (subDir==true){
						inputDirFile = new File(propertiesReader.getPropValues("fileDirectory")).listFiles()?.sort { it.lastModified() }?.head().listFiles()?.sort { it.lastModified() }?.head()
						//dataSourceType = new file().getPathPart(new File(new file().getPathPart(inputDirFile, 1, '\\\\')), 1, '\\.')
						annotator.setDatasourceType(dsType)
						println "datasourceType : "+annotator.getDatasourceType()
						annotator.annotateAndRename(annotationPlan, procedure, xmlDir, anotatedDocDir, OpeSystem)
						annotator.concatenateLuxFiles(anotatedDocDir)
					}else{
						println "can't annotate and rename files : the input directory has no subdirectory"
					}
				}
				else{
					inputDirFile = new File(propertiesReader.getPropValues("fileDirectory")).listFiles()?.sort { it.lastModified() }?.head()
					//dataSourceType = new file().getPathPart(new File(new file().getPathPart(inputDirFile, 1, '\\\\')), 1, '\\.')
					annotator.setDatasourceType(dsType)
					println "datasourceType : "+annotator.getDatasourceType()
					annotator.annotate(annotationPlan, procedure, xmlDir, anotatedDocDir)
				}

			}else{
				println "error : The output path must be diferent from the input one"
			}
		}
		
		//postprocess
		
		if (propertiesReader.getPropValues("extractDataFromLux") == "true"){
			def anotatedDocDir = new File(propertiesReader.getPropValues("anotatedDocDirectory"))
			def ExtactedDataFile = new File(propertiesReader.getPropValues("extractedDataFile"))
			println propertiesReader.getPropValues("extractedDataFile")
			def FreqFile = new File(propertiesReader.getPropValues("extractedDataFrequencyFile"))
			new lux().extractNamedEntity(anotatedDocDir,ExtactedDataFile, FreqFile)
		}
		if (propertiesReader.getPropValues("compareTwoSets")=="true"){
			def ExtactedDataFile = new File(propertiesReader.getPropValues("extractedDataFile"))
			def refEntityFile = new File(propertiesReader.getPropValues("refEntityFile"))
			def discoveredEntities = new File(propertiesReader.getPropValues("discoveredEntities"))
			def distanceThreshold = propertiesReader.getPropValues("allowedDistanceThreshold") as int
			def freq = propertiesReader.getPropValues("getFrequencies")
			def comparision = new txt().compareExtractedData(ExtactedDataFile, refEntityFile, distanceThreshold, discoveredEntities, freq)
		}
		if (propertiesReader.getPropValues("removeSplittedFiles")=="true"){
			new xml().removeFile(new File(propertiesReader.getPropValues("fileDirectory")))
		}
	}

}
