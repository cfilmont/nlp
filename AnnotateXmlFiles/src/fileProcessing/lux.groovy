package fileProcessing


import groovyx.net.http.RESTClient
import groovyx.net.http.ParserRegistry
import groovy.io.*
import groovyx.net.http.HTTPBuilder
import groovyx.net.http.HttpResponseDecorator
import static groovyx.net.http.ContentType.*
import static groovyx.net.http.Method.*
import groovy.xml.*

class lux extends xml {
	String machineId


	def getMachineId(){
		return this.machineId
	}
	def setMachineId(MachineId){
		this.machineId = MachineId
	}


	def extractNamedEntity(inputDir, outputFile, FreqoutputFile){
//		new file().checkDir(outputFile)
		def list = []
		println "outputFile : "+outputFile.path
		inputDir.eachFileRecurse (FileType.FILES) { file -> list << file }
		outputFile.write("")
		list.each {
			println 'extracting data from ' + it
			def parsedDoc = new XmlSlurper().parse(it)
			parsedDoc.doc.knowledge.descriptors.descriptor.each{
				outputFile.append(it.@name,'utf-8')
				outputFile.append('\n','utf-8')
			}
		}
		def freqOutputFile = new txt().countExtractedData(outputFile, FreqoutputFile)
	}
	
	def removeNode(inputFile, nodeToRemoveList){
		println 'removing node from '+ new file().getPathPart(inputFile, 1, '\\\\')
		def annotatedDoc = new XmlSlurper().parse(inputFile)
		for (node in nodeToRemoveList){
			annotatedDoc.doc.knowledge.descriptors.descriptor.a.findAll{it.@name.text() == node}*.replaceNode{}
		}
		annotatedDoc.doc.knowledge.descriptors.descriptor.a.findAll{it.@refType.text() == "/List/BaseURI"}*.replaceNode{}
		return XmlUtil.serialize(new StreamingMarkupBuilder().bind {mkp.yield annotatedDoc})
	}
	
	def concatenateLuxFiles(inputDir){
		def inputdirList = []
		inputDir.eachFileRecurse (FileType.DIRECTORIES) { file -> inputdirList << file }
		def inputdirSize = inputdirList.size()
		inputdirList.each{
			println "concatenating file towards "+it.path
			def inputfileList = []
			def outputFile = new File( it.path.toString()+".lux")
			it.eachFileRecurse (FileType.FILES) { file -> inputfileList << file }
			def indexFile = 0
			for (file in inputfileList){
				new File (file.toString()).withReader{ r ->
					outputFile.append(r, 'utf-8')
				}
				indexFile++
			}
			it.deleteDir()
			outputFile.write(outputFile.text.replaceAll("\\<\\?xml[^\\>]+\\>", ''), 'utf-8')
			outputFile.write(outputFile.text.replaceAll("\\<lux\\>", ''), 'utf-8')
			outputFile.write(outputFile.text.replaceAll("\\</lux\\>", ''), 'utf-8')
			outputFile.append("</lux>", 'utf-8')
			def sOuputFile = outputFile.getText()
			def wellFormedOutputFile = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n<lux>"+ sOuputFile
			outputFile.write(wellFormedOutputFile, 'utf-8')
		}
	}
	//required : the input file(s) must be placed in a directory inside the input one
	//the input is xml but it is converted by the first component of the annotation plan because Luxid doesnot annotate xml files
	def annotateAndRename(scaname,procedure,inputDir,outputDir, OpeSystem){
		new file().checkDir(outputDir)
		ParserRegistry.setDefaultCharset("utf-8")
		def inputDirList = []
		inputDir.eachFileRecurse (FileType.DIRECTORIES) { file -> inputDirList << file }
		inputDirList.each{
			def inputDirName = new file(OS : OpeSystem)
			if (inputDirName.getOS()=="linux"){
				inputDirName = inputDirName.getPathPart(it, 1,'\\/')
			}
			else if (inputDirName.getOS()=="windows"){
				inputDirName = inputDirName.getPathPart(it, 1,'\\\\')
			}
			else{
				println "couldnot recognise OS"
			}
			println "processing "+inputDirName+ " folder before renaming output files"
			def inputFileList = []
			it.eachFileRecurse (FileType.FILES) { file -> inputFileList << file }
			def asx = new RESTClient( 'http://'+machineId+':8091/temis/v1/annotation/' )
			// Fix a problem with GZIP content encoding
			asx.contentEncoding = "identity"
			def outputDirectory = new File(outputDir.toString()+"/"+inputDirName)
			new file().checkDir(outputDirectory)
			for (def inputFile in inputFileList) {
				def fileName = new file().getPathPart(new File(inputFile.getName()), 2, '\\.')
				def	outputFile = new File(outputDirectory.toString()+"/"+fileName+".lux")
				
				def rep = callRest(asx,scaname,procedure,inputFile)
				writeOutputLux(rep, outputFile, fileName)
			}
		}
	}

	def annotate(scaname,procedure,inputDir,outputDir){
		new file().checkDir(outputDir)
		ParserRegistry.setDefaultCharset("utf-8")
		def inputFileList = []
		inputDir.eachFileRecurse (FileType.FILES) { file -> inputFileList << file }
		inputFileList.each{
			println "processing "+it
			def asx = new RESTClient( 'http://'+machineId+':8091/temis/v1/annotation/' )
			// Fix a problem with GZIP content encoding
			asx.contentEncoding = "identity"
			def fileName = new file().getPathPart(new File(it.getName()), 2, '\\.')
			def outputFile = new File(outputDir.toString()+"/"+fileName+".lux")
			def rep = callRest(asx,scaname,procedure,it)
			writeOutputLux(rep, outputFile, fileName)

		}
	}

	def callRest(asx, scaname, procedure, txt){
		def response
		switch (this.datasourceType){
			case 'xml':
				def txtfile=new File(txt.path)
				def txtbytes = txtfile.readBytes()
				try{
					response = asx.post( path : "annotate/${scaname}.xml",
					query : [p : procedure, f : txtfile.canonicalPath],
					body : txtbytes,
					contentType : TEXT,
					requestContentType : BINARY )
					return response
				}catch (Exception e){
					e.printStackTrace()
			}
			case 'html':
				def htmlstring = new File(txt.path).getText('UTF-8')
				try{
					response = asx.post( path : "annotate/${scaname}.xml",
					query : [p : procedure],
					body : htmlstring,
					contentType : TEXT,
					requestContentType : HTML )
					return response
				}catch (Exception e){
					e.printStackTrace()}
		}
	}



	def writeOutputLux(response, outputFile, fileName){
		def lux = response.data
		if (response.status != 200 && response.status != 204) {
			println lux
		} else {
			println 'annotating '+fileName
			if (this.getRemoveOutputNodes() == "true"){
				outputFile.write(lux.text, 'utf-8')
				def nodeToRemove=["hierarchy", "broader", "inScheme", "StopTerms.french", "ForbiddenSpans.french"]
				outputFile.write(new lux().removeNode(outputFile, nodeToRemove), 'utf-8')
			}else{
				outputFile.write(lux.text, 'utf-8')
			}
		}
	}
}
