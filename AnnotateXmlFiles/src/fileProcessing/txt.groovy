package fileProcessing


import groovy.lang.Closure
import java.lang.String
import java.nio.charset.Charset
import org.apache.commons.lang.StringUtils
import java.lang.Math

class txt extends file{
	String datasourceType='txt'

	def getProgramConfig(argList){
		def now = new Date()
		println "running LaunchAnnotation\n : "+now.getAt(Calendar.DAY_OF_MONTH)+"/"+now.month+"/"+now.year+"\n"+now.getHours()+":"+now.minutess
		argList.each { entry -> println "$entry.key : $entry.value" }
	}
	def getProgramConfig(argList, outputFile){
		def now = new Date()
		outputFile.write("running LaunchAnnotation\n : "+now.getAt(Calendar.DAY_OF_MONTH)+"/"+now.month+"/"+now.year+"\n"+now.getHours()+":"+now.minutes)
		argList.each { entry ->
			outputFile.append("$entry.key : $entry.value\n")
		}
	}
	def readArgFile(File,argList){
		File.eachLine { line ->
			def (paramName, paramValue) = line.tokenize( ' ' )
			argList.put(paramName, paramValue)
		}
	}
	//to be improved : get the same method as Luxid 'fuzzy matching' : the first x caracters must be the same, and the following may vary
	def compareExtractedData(inputFile, inputFileRef, distanceThreshold, OutputFile, freq){
		println "comparing two sets of data"
		println 'looking for entities that are in '+inputFile.path+' but not in '+inputFileRef.path
		def extractedNamedEntities=[]
		inputFile.eachLine { line ->
			extractedNamedEntities.add(line)
		}
		def extractedNamedEntitiesRef=[]
		inputFileRef.eachLine { line ->
			extractedNamedEntitiesRef.add(line)
			println "reference person named entities "+line
		}
		OutputFile.write("")
		extractedNamedEntities.each{
			if (!extractedNamedEntitiesRef.contains(it)){
				OutputFile.append(it+"\n",'utf-8')
			}
		}
		def specificTMCount=0
		def totalTMCount=0
		def content = OutputFile.getText('utf-8')
		OutputFile.eachLine{ line->
			totalTMCount++
			for (ref in extractedNamedEntitiesRef.unique()){
				def distance = new StringUtils().getLevenshteinDistance(ref, line)
				if (distance <= distanceThreshold){
					content = content.replace(line,"")
					content = content.replace("[\r\n]+","")
					specificTMCount++
				}
			}
		}
		println "specificTMCount "+specificTMCount
		println "totalTMCount "+totalTMCount
		def TMGain = Math.round(specificTMCount*100/totalTMCount)
		def TMGainReport = "there are "+specificTMCount+" person named entities that are specific to TM360 extraction out of "+totalTMCount+" person named entities, that is "+TMGain+" %\n"
		OutputFile.write(TMGainReport, 'utf-8')
		OutputFile.append(content,'utf-8')
		if (freq =="true"){
			def freqOutputFile = new File(OutputFile.path.minus(new file().getPathPart(OutputFile, 1, '\\.'))+"csv")
			freqOutputFile.write(TMGainReport)
			countExtractedData(OutputFile,freqOutputFile)
			OutputFile.delete()
		}else{
			sortLine(OutputFile, OutputFile)
		}
	}


	//to be improved : the user could be asked to choose the sort parameter (either alphabetical either by frequency)
	def countExtractedData(inputFile, freqOutputFile){
		println 'computing frequency of extracted data from '+inputFile
		freqOutputFile.write("")
		def splittedOutputPath = inputFile.toString().tokenize('\\')
		def inputFileEnd = splittedOutputPath[splittedOutputPath.size-1]
		def inputDir = ''
		for (def i =0 ; i<splittedOutputPath.size()-1; i++){
			inputDir= inputDir+splittedOutputPath[i]+'/'
		}
		def list = new File(inputDir, inputFileEnd).collect {it}
		def wFrequency = list.groupBy().collectEntries { [(it.key) : it.value.size()] }
		wFrequency.each{ word, freq -> freqOutputFile.append("${word};${freq}\n") }
	}

	def sortLine(inputFile, sortedFile){
		def lineList = []
		inputFile.eachLine {line ->
			lineList.add(line)
		}
		sortedFile.write("")
		Collections.sort(lineList)
		lineList.each{ sortedFile << it+"\n" }

	}
	def removeDuplicate(file){
		def uniqueList = []
		file.eachline{line->
			uniqueList.add(line)
		}
		uniqueList.unique
		file.write(uniqueList.unique)
	}
}
