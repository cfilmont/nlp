package fileProcessing;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;
import java.io.FileReader;

public class properties {
	private static final String RELATIVE_PATH_TO_PROPERTIES = "config-serveur-all-Mesh.properties";

	// FileInputStream inputStream;

	public static String getPropValues(String propName) throws IOException {
		String result = null;

		try {
			Properties prop = new Properties();
			
			String propFileName = "config.properties";
			//InputStream fileStream = new FileInputStream("resources/config.properties");
			ClassLoader classLoader = Thread.currentThread().getContextClassLoader();
			InputStream input = classLoader.getResourceAsStream(RELATIVE_PATH_TO_PROPERTIES);
			//inputStream = properties.class.getResourceAsStream(propFileName);

			if (input != null) {
				prop.load(input);
			} else {
				throw new FileNotFoundException("property file '"+ propFileName + "' not found in the classpath");
			}

			// get the property value and print it out
			result = prop.getProperty(propName);
		} catch (Exception e) {
			System.out.println("Exception: " + e);
		} finally {
			//fileStream.close();
		}
		return result;
		
		

		
	}
}