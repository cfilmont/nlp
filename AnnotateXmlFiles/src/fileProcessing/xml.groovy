package fileProcessing

import groovy.xml.*
import static groovy.io.FileType.FILES

class xml extends file{

	def removeOutputNodes
	def renameOutputFile
	
	def getRemoveOutputNodes(){
		return this.removeOutputNodes
	}
	def setRemoveOutputNodes(rmOutputNodes){
		this.removeOutputNodes = rmOutputNodes
	}
	
	def getRenameOutputFile(){
		return this.renameOutputFile
	}
	def setRenameOutputFile(mvOutputFile){
		this.renameOutputFile = mvOutputFile
	}
	
	
	def cleanFile(dir, expressionToRemove){
		def inputFileList =[]
		dir.eachFileMatch (~/.*.xml/) { file -> inputFileList << file }
		inputFileList.each{
			println inputFileList.size()
			println inputFileList
			def content = it.getText('UTF-8')
			content = content.replace(expressionToRemove, " ")
			it.write(content,'utf8')
		}
	}
	def splitFile(file,rootNode, splitNode, outputDir){
		new file().checkDir(new File(outputDir))
		println "splitting "+file.path+ " into several files"
		new XmlSlurper().parseText(file.getText())."${splitNode}".eachWithIndex { element, index ->
			new File(outputDir+"/File${ "${index+1}".padLeft( 4, '0' ) }.xml").withWriter { w ->
				w << XmlUtil.serialize(new StreamingMarkupBuilder().bind {
					"${rootNode}" {
						mkp.yield element
					}
				} )
			}
		}
	}
	
}
