package fileProcessing

import java.io.File;
import java.io.IOException;
import org.apache.commons.io.FileUtils.*

class file {
	String OS
	String datasourceType
	def getDatasourceType(){
		return this.datasourceType
	}
	def setgetDatasourceType(DTSource){
		this.datasourceType = DTSource
	}
	
	def getOS(){
		return this.OS
	}
	def setOS(operatingSystem){
		this.OS = operatingSystem
	}
	
	def checkDir(dir){
		if(!dir.exists()) {
			dir.mkdir()
		}
	}
	def getPathPart(file, part, separator){
		def splittedPath = file.path.split(separator)
		return splittedPath[splittedPath.length-part]
	}
	def removeFile(fileToRemove){
		for (File file: fileToRemove.listFiles()) file.delete();
	}
}
