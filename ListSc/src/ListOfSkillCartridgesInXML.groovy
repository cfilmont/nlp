@Grab(group='org.codehaus.groovy.modules.http-builder', module='http-builder', version='0.7.1')
import groovyx.net.http.RESTClient
import static groovyx.net.http.ContentType.*
import groovyx.net.http.ParserRegistry

//ParserRegistry.setDefaultCharset("utf-8")

def asx = new RESTClient( 'http://localhost:8091/temis/v1/annotation/' )
// Fix a problem with GZIP content encoding
asx.contentEncoding = "identity"

// Option 1: Get an XML String output
def response = asx.get( path : 'cartridges.xml',
						contentType : TEXT)
def resp = response.data
if (response.status != 200 && response.status != 204) {
  println resp
} else {
  println resp.text
}

// Option 2: Get an XML object parsed with XMLSlurper
/*response = asx.get( path : 'cartridges.xml',
					contentType : XML )
resp = response.data

if (response.status != 200 && response.status != 204) {
  println resp
} else {
  resp.cartridge.each() { c ->
	  println c
  }
}*/

