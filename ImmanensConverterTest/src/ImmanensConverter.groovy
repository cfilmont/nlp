import java.util.List
import java.io.File;

import groovy.util.slurpersupport.GPathResult
import groovy.xml.MarkupBuilder
import groovy.xml.StreamingMarkupBuilder
import groovy.xml.XmlUtil

import com.temis.lux.LuxDoc
import com.temis.lux.util.LuxDocHelper
import com.temis.tsl.AnnotatorInfo
import com.temis.tsl.AnnotatorParam
import com.temis.tsl.AnnotatorProc
import com.temis.tsl.ApplicationException
import com.temis.tsl.DefaultAnnotator
import com.temis.tsl.Description
import com.temis.tsl.Descriptions
import com.temis.tsl.DocumentContext
import com.temis.tsl.DocumentFormat
import com.temis.tsl.util.FileUtils
import com.temis.tsl.AnnotatorTester


@AnnotatorInfo(description="Converts an Immanens XML file into LUX", version="1.0")
class ImmanensConverter extends DefaultAnnotator {
/*	static main(args){
		println 'hello'*/
		//ImmanensConverter ic = new ImmanensConverter()
		//ic.init()
		// Input directory path
		//def txtfileDir = new File(args[0])
/*		def outputTxtfileDir = new File(args[1])
		def list = []
		ic.getClassMethods()

		def txtfile = new File("C:/Users/cfilmont/Documents/projets/Sc_et_vie/corpora/test_conversion/input/test.xml")
		//def castedtxtfile= txtfile.asType(DocumentContext)
		println txtfile.getClass()
		def convertedDoc = new ArrayList<DocumentContext>();
		convertedDoc = ic.multiply(txtfile)*/
		
		//txtfileDir.eachFileRecurse (FileType.FILES) { file -> list << file }
/*		list.each{
			//def docContext = DocumentContext(it)
			println it.getClass()
			println it.public List getClassMethods() {
				// TODO Auto-generated method stub
				return super.getClassMethods();
			}
			try{
				outputTxtfileDir = ic.multiply(it)
			}catch (Exception e){
				throw new Exception(e)
			}
		}*/

/*		new File(args[2]).eachLine { line ->
			println line
		  }*/
		
/*		AnnotatorTester tester = new AnnotatorTester()
		// annotator to be tested
		ImmanensConverter annotator = new ImmanensConverter()
		// associate annotator tester and annotator
		//annotator.init()
		tester.setAnnotator(annotator)
		// set input file
		tester.setInput(new File(args[0]  + ".xml"))
		// set outpout file
		tester.setOutput(new File(args[1]+  "-out.lux"))
		// process
		tester.process()*/
/*		def convertedDoc = new ArrayList<DocumentContext>();
		def docContext = new DocumentContext()
		convertedDoc = multiply(txtfileDir.asType(docContext))*/
	//}


	@AnnotatorParam(label="Id generator", description="Which policy to adopt to generate the document identifier", possibleValues=["UUID", "IDAX", "identifier"])
	public String idGenerator = "identifier";
	XmlSlurper slurper
	@AnnotatorParam(label="Language", description="Language of the documents")
	public String language = "french";
	private StreamingMarkupBuilder smb;

	public void init() {
		this.smb = new StreamingMarkupBuilder()
		this.smb.encoding = "utf-8";
		//def saxparser = new com.sun.org.apache.xerces.internal.parsers.SAXParser();
		//saxparser.setFeature("http://apache.org/xml/features/disallow-doctype-decl",false);
		this.slurper = new XmlSlurper(false,false)
	}


	@AnnotatorProc(name = "convert", tags = "convert")
	@Descriptions([
		@Description(lang = "en", label = "Immanens XML files", text = "Converts Immanens XML files into LUX"),
		@Description(lang = "fr", label = "Fichiers XML Immanens", text = "Convertit des fichiers au format XML Immanens en LUX")
	])
	public List<DocumentContext> multiply(DocumentContext ctx) throws ApplicationException {
		try {
			LuxDoc doc = ctx.getLuxDoc()
			LuxDocHelper docHelper = new LuxDocHelper(doc);
			def result = new ArrayList<DocumentContext>();
			def inputstream = docHelper.getRawInputStream();
			if (inputstream != null) {
				def entity = slurper.parse(inputstream)
				def newCtx = ctx.emptyClone()
				newCtx.setDocument(handleImmanensEntity(entity), DocumentFormat.luxxmlstring)
				result.add(newCtx);
			}
			return result
		} catch (Exception e) {
			throw new ApplicationException(e)
		}
	}

	String handleImmanensEntity(def entity) {
		def writer = new StringWriter()
		def docid = null;
		switch (this.idGenerator) {
			case 'UUID':
				docid = java.util.UUID.randomUUID();
				break;
			case 'IDAX':
				docid = entity.pp_metadata.IDAX.text();
				break;
			case 'identifier':
				docid = entity.entityMetadata.identifier.text();
				break;
		}
		def itemIndex = 1
		new MarkupBuilder(writer).doc(id:docid) {
			metadata {
				a(name:'IDAX', 			value:entity.pp_metadata.IDAX.text())
				a(name:'PDF_pages', 	value:entity.pp_metadata.PDF_pages.text())
				a(name:'SEAL', 			value:entity.pp_metadata.SEAL.text())
				a(name:'identifier', 	value:entity.entityMetadata.identifier.text())
				a(name:'EntityTitle', 	value:entity.entityMetadata.title.text())
				a(name:'section', 		value:entity.entityMetadata.section.text())
				a(name:'subSection', 	value:entity.entityMetadata.subSection.text())
				a(name:'type', 			value:entity.entityMetadata.type.text())
				a(name:'language', 		value:language)
			}
			metadata (zone:'EntityTitle') {
				a(name:'language', 		value:language)
			}
			content(zone:'EntityTitle') {
				text(entity.entityMetadata.title.text())
			}
			entity.item.each { item ->
				if (item.itemMetadata.type.text() == 'text') {
					metadata (zone:"item_${itemIndex}_content") {
						a(name:"identifier",						value:item.itemMetadata.identifier.text())
					}
					content (zone:"item_${itemIndex}_content") {
						text(convertToXHtmlString(item.channel.content.div))
					}
					metadata (zone:"item_${itemIndex}_title") {
						a(name:"language", 						value:language)
					}
					content (zone:"item_${itemIndex}_title") {
						text(item.itemMetadata.title.text()) {
						}
						metadata (zone:"item_${itemIndex}_toptitle") {
							a(name:"language", 						value:language)
						}
						content (zone:"item_${itemIndex}_toptitle") {
							text(item.itemMetadata.toptitle.text())
						}
						metadata (zone:"item_${itemIndex}_header") {
							a(name:"language", 						value:language)
						}
						content (zone:"item_${itemIndex}_header") {
							text(item.itemMetadata.header.text())
						}
						itemIndex++
					}
				}
			}
		}
		return writer.toString();
	}
	private String convertToXHtmlString(GPathResult node) {
		return smb.bindNode(node);
	}
}

class ImmanensConverter {

}
